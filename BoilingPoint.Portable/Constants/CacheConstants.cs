﻿using System;

namespace BoilingPoint.Portable.Constants
{
    public static class CacheConstants
    {
        public static readonly DateTime NoAbsoluteExpiration = DateTime.MaxValue;
        public static readonly TimeSpan NoSlidingExpiration = TimeSpan.Zero;
    }
}
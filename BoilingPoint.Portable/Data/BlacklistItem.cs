﻿namespace BoilingPoint.Portable.Data
{
    public class BlacklistItem : BaseEntity
    {
        public string UserScreenName { get; set; }

        protected bool Equals(BlacklistItem other)
        {
            return string.Equals(UserScreenName, other.UserScreenName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((BlacklistItem) obj);
        }

        public override int GetHashCode()
        {
            return (UserScreenName != null ? UserScreenName.GetHashCode() : 0);
        }

        public override string ToString()
        {
            return UserScreenName;
        }
    }
}
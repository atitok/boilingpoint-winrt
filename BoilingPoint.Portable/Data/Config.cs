﻿namespace BoilingPoint.Portable.Data
{
    public class Config : BaseEntity
    {
        public string AppBaseUrl { get; set; }

        public long UpdateIntervalInMinutes { get; set; }

        public string DoubleGisApiKey { get; set; }
    }
}
﻿using BoilingPoint.Portable.Enums;

namespace BoilingPoint.Portable.Data
{
    public class Device
    {
        public string Id { get; set; }
        public PlatformType PlatformType { get; set; }
        public string ChannelUrl { get; set; }
    }
}
﻿namespace BoilingPoint.Portable.Data
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
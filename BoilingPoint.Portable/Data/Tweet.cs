﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BoilingPoint.Portable.Attributes;
using BoilingPoint.Portable.Data.TweetEntities;
using BoilingPoint.Portable.Extensions;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Data
{
    /// <summary>
    ///     Tweet.
    /// </summary>
    public class Tweet : BaseEntity
    {
        #region Fields

        public const int MAX_LENGTH = 140;

        public const String HATE_TO_WAIT_TAG = "ihatetowait";
        public const String PEOPLE_INLINE_TAG = "inline";
        public const String WAIT_TIME_TAG = "towait";
        public const String CASHBOX_AVAILABLE_TAG = "avail";
        public const String CASHBOX_CLOSED_TAG = "closed";

        private const string TIME_PATTERN = @"(?:(?<h>\d+)h)?(?:(?<m>\d+)m)?";
        private const string PEOPLE_INLINE_PATTERN = @"(\d+)inline";
        private const string WAIT_TIME_PATTERN = @"((?:\d+(?:m|h))+)towait";
        private const string CASH_BOX_AVAILABLE_PATTERN = @"(\d+)avail";
        private const string CASHBOX_CLOSED_PATTERN = @"(\d+)closed";

        private string _address;

        private Bank _bank;
        private int _cashboxAvailable;
        private int _cashboxClosed;
        private string _comment;
        private int _createdAtTimestamp;
        private Filial _filial;
        private GeoPoint _location;
        private int _peopleInline;
        private Profile _profile;
        private string _text;
        private double _waitTime;

        #endregion

        #region Ctors

        public Tweet()
        {
            Tags = new List<String>();
            Users = new List<User>();
            UserScreenNames = new List<string>();
            MediaEntities = new List<MediaEntity>();
        }

        public Tweet(JToken token)
            : this()
        {
            Initialize(token);
        }

        #endregion

        #region Properties

        public String Address
        {
            get { return _address; }
            set
            {
                _address = value;
                NotifyOfPropertyChange();
            }
        }

        [Ignore]
        public Filial Filial
        {
            get { return _filial; }
            set
            {
                _filial = value;
                NotifyOfPropertyChange();
            }
        }

        public string ProfileId
        {
            get { return _profile != null ? _profile.Id : null; }
            set
            {
                if (_profile == null)
                    _profile = new Profile();
                _profile.Id = value;
            }
        }

        [Ignore]
        public Profile Profile
        {
            get { return _profile; }
            set
            {
                _profile = value;
                NotifyOfPropertyChange();
            }
        }

        [Ignore]
        public Bank Bank
        {
            get { return _bank; }
            set
            {
                _bank = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("BankTitle");
            }
        }

        public string BankId
        {
            get { return _bank != null ? _bank.objectId : null; }
            set
            {
                if (_bank == null)
                    _bank = new Bank();
                _bank.objectId = value;
            }
        }

        public string BankTitle
        {
            get { return _bank != null ? _bank.Title : string.Empty; }
            set
            {
                if (_bank == null)
                    _bank = new Bank();
                _bank.Title = value;
            }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                NotifyOfPropertyChange();
            }
        }

        public Int32 PeopleInline
        {
            get { return _peopleInline; }
            set
            {
                _peopleInline = value;
                NotifyOfPropertyChange();
            }
        }

        [Ignore]
        [JsonIgnore]
        public string HateToWaitTag
        {
            get { return string.Format("#{0}", HATE_TO_WAIT_TAG); }
        }

        [Ignore]
        [JsonIgnore]
        public string PeopleInlineTag
        {
            get { return string.Format("#{0}inline", PeopleInline); }
        }

        public double WaitTime
        {
            get { return _waitTime; }
            set
            {
                _waitTime = value;
                NotifyOfPropertyChange();
            }
        }

        [Ignore]
        [JsonIgnore]
        public string WaitTimeTag
        {
            get { return string.Format("#{0}towait", FormatTime(TimeSpan.FromMinutes(WaitTime))); }
        }

        public Int32 CashBoxAvailable
        {
            get { return _cashboxAvailable; }
            set
            {
                _cashboxAvailable = value;
                NotifyOfPropertyChange();
            }
        }

        [Ignore]
        [JsonIgnore]
        public string CashBoxAvailableTag
        {
            get { return string.Format("#{0}avail", CashBoxAvailable); }
        }

        public Int32 CashBoxClosed
        {
            get { return _cashboxClosed; }
            set
            {
                _cashboxClosed = value;
                NotifyOfPropertyChange();
            }
        }

        [Ignore]
        [JsonIgnore]
        public string CashBoxClosedTag
        {
            get { return string.Format("#{0}closed", CashBoxClosed); }
        }

        public ulong ID { get; set; }

        public String UserID { get; set; }

        public String UserName { get; set; }

        public String UserScreenName { get; set; }

        public String ProfileImageUrl { get; set; }

        public String Text
        {
            get { return _text ?? (_text = ToString(true)); }
            set { _text = value; }
        }

        public GeoPoint Location
        {
            get { return _location; }
            set
            {
                _location = value;
                Latitude = _location.Latitude;
                Longitude = _location.Longitude;
            }
        }

        [Ignore]
        public Double Latitude { get; set; }


        [Ignore]
        public Double Longitude { get; set; }

        public String CountryCode { get; set; }

        [Ignore]
        public DateTime CreatedAt { get; set; }

        public int CreatedAtTimestamp
        {
            get { return _createdAtTimestamp; }
            set
            {
                _createdAtTimestamp = value;
                if (Equals(CreatedAt, default(DateTime)))
                {
                    createdAt = Utils.UnixTimeStampToDateTime(CreatedAtTimestamp);
                }
            }
        }

        public List<String> Tags { get; set; }

        [Ignore]
        public List<User> Users { get; set; }

        public List<string> UserScreenNames { get; set; }

        [Ignore]
        public List<MediaEntity> MediaEntities { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return ToString(true);
        }

        public string ToShortString()
        {
            return ToString(false);
        }

        public string ToString(bool includeServiceTags)
        {
            var sb = new StringBuilder();

            if (ID == 0)
            {
                if (Bank != null)
                {
                    if (Bank.TwitterUsername != null)
                    {
                        sb.AppendFormat("@{0} ", Bank.TwitterUsername);
                    }
                    else
                    {
                        sb.AppendFormat("#{0} ", Utils.GetTitleCaseHashtag(Bank.Title));
                    }
                }

                foreach (User user in Users)
                {
                    sb.AppendFormat("@{0} ", user.ScreenName);
                }

                if (includeServiceTags)
                {
                    sb.AppendFormat("{0} ", HateToWaitTag);

                    if (PeopleInline > 0)
                    {
                        sb.AppendFormat("{0} ", PeopleInlineTag);
                    }

                    if (WaitTime > 0)
                    {
                        sb.AppendFormat("{0} ", WaitTimeTag);
                    }

                    if (CashBoxAvailable > 0)
                    {
                        sb.AppendFormat("{0} ", CashBoxAvailableTag);
                    }

                    if (CashBoxClosed > 0)
                    {
                        sb.AppendFormat("{0} ", CashBoxClosedTag);
                    }
                }

                foreach (string tag in Tags)
                {
                    sb.AppendFormat("#{0} ", tag);
                }

                if (!string.IsNullOrEmpty(Comment))
                {
                    sb.AppendFormat(Comment.Trim());
                }
            }
            else
            {
                sb.Append(_text);

                if (!includeServiceTags)
                {
                    Utils.ReplaceIgnoreCase(sb, HateToWaitTag, string.Empty);
                    Utils.ReplaceIgnoreCase(sb, PeopleInlineTag, string.Empty);
                    Utils.ReplaceIgnoreCase(sb, WaitTimeTag, string.Empty);
                    Utils.ReplaceIgnoreCase(sb, CashBoxAvailableTag, string.Empty);
                    Utils.ReplaceIgnoreCase(sb, CashBoxClosedTag, string.Empty);

                    var regex = new Regex(@"[ ]{2,}", RegexOptions.None);
                    sb = new StringBuilder(regex.Replace(sb.ToString(), @" "));
                }
            }

            return sb.ToString().Trim();
        }

        protected String FormatTime(TimeSpan time)
        {
            String result = String.Empty;

            if (time.Hours > 0)
            {
                result = String.Format("{0}h", time.Hours);
            }

            if (time.Minutes > 0)
            {
                result = String.Format("{0}{1}m", result, time.Minutes);
            }

            return result;
        }

        protected double RecognizeTime(String timeStr)
        {
            TimeSpan time = TimeSpan.Zero;

            var regex = new Regex(TIME_PATTERN);
            Match match = regex.Match(timeStr);

            Group hGroup = match.Groups["h"];
            Group mGroup = match.Groups["m"];

            if (hGroup != null)
            {
                Double timeVal;
                if (Double.TryParse(hGroup.Value, out timeVal))
                {
                    time = time.Add(TimeSpan.FromHours(timeVal));
                }
            }

            if (mGroup != null)
            {
                Double timeVal;
                if (Double.TryParse(mGroup.Value, out timeVal))
                {
                    time = time.Add(TimeSpan.FromMinutes(timeVal));
                }
            }

            return time.TotalMinutes;
        }

        protected T GetTagValue<T>(string tag, String pattern)
        {
            T result = default(T);

            if (String.IsNullOrEmpty(tag)) return result;
            Match match = Regex.Match(tag, pattern);
            if (!match.Success || match.Groups.Count <= 1) return result;
            String tagValue = match.Groups[1].Value;

            result = (T) Convert.ChangeType(tagValue, typeof (T), CultureInfo.CurrentCulture);

            return result;
        }

        public void Initialize(JToken token)
        {
            ID = token.GetValueOrDefault<ulong>("StatusID");
            UserID = token.SelectToken("User.Identifier").GetValueOrDefault<string>("UserID");
            UserScreenName = token.SelectToken("User.Identifier").GetValueOrDefault<string>("ScreenName");
            UserName = token.SelectToken("User").GetValueOrDefault<string>("Name");
            ProfileImageUrl = token.SelectToken("User").GetValueOrDefault<string>("ProfileImageUrl");
            Text = token.GetValueOrDefault<string>("Text");

            CreatedAt = token.GetValueOrDefault<DateTime>("CreatedAt");
            CreatedAtTimestamp = Utils.DateTimeToUnixTimestamp(token.GetValueOrDefault<DateTime>("CreatedAt"));

            JToken coordinates = token.SelectToken("Coordinates");
            Location = new GeoPoint(coordinates.GetValueOrDefault<double>("Latitude"),
                                    coordinates.GetValueOrDefault<double>("Longitude"));

            InitializeTags(token);

            InitializeUsers(token);

            InitializeMediaEntities(token);
        }

        private void InitializeMediaEntities(JToken token)
        {
            JToken mediaEntitiesToken = token.SelectToken("Entities.MediaEntities");
            if (mediaEntitiesToken == null)
                return;

            foreach (JToken mediaEntityToken in mediaEntitiesToken)
            {
                var mediaEntity =
                    new MediaEntity
                        {
                            ID = mediaEntityToken.GetValueOrDefault<ulong>("Id"),
                            MediaUrl = mediaEntityToken.GetValueOrDefault<string>("MediaUrl"),
                            Type = mediaEntityToken.GetValueOrDefault<string>("Type")
                        };

                var sizesToken = mediaEntityToken.Value<JToken>("Sizes");

                if (sizesToken == null)
                    continue;

                mediaEntity.Sizes = new List<PhotoSize>();
                foreach (JToken sizeToken in sizesToken)
                {
                    var photoSize =
                        new PhotoSize
                            {
                                Type = sizeToken.GetValueOrDefault<string>("Type"),
                                Width = sizeToken.GetValueOrDefault<int>("Width"),
                                Height = sizeToken.GetValueOrDefault<int>("Height")
                            };

                    mediaEntity.Sizes.Add(photoSize);
                }

                MediaEntities.Add(mediaEntity);
            }
        }

        private void InitializeUsers(JToken token)
        {
            JToken userMentionEntitiesToken = token.SelectToken("Entities.UserMentionEntities");
            if (userMentionEntitiesToken == null) return;
            foreach (JToken userMentionEntityToken in userMentionEntitiesToken)
            {
                var user = new User
                               {
                                   Name = userMentionEntityToken.GetValueOrDefault<string>("Name"),
                                   ScreenName = userMentionEntityToken.GetValueOrDefault<string>("ScreenName")
                               };

                Users.Add(user);
            }

            UserScreenNames = Users.Select(x => x.ScreenName).ToList();
        }

        private void InitializeTags(JToken token)
        {
            const string w8TagsPath = "Entities.HashTagMentions";
            const string wpTagsPath = "Entities.HashTagEntities";

            JToken hastTagEntitiesToken = token.SelectToken(wpTagsPath);
            if (hastTagEntitiesToken == null)
            {
                hastTagEntitiesToken = token.SelectToken(w8TagsPath);
            }

            if (hastTagEntitiesToken == null)
            {
                return;
            }

            foreach (JToken hashTagEntityToken in hastTagEntitiesToken)
            {
                var tag = hashTagEntityToken.GetValueOrDefault<string>("Tag");
                if (!string.Equals(tag, HATE_TO_WAIT_TAG, StringComparison.OrdinalIgnoreCase))
                {
                    AddTag(tag);
                }
            }
        }

        public void AddTag(string tag)
        {
            if (Regex.IsMatch(tag, CASHBOX_CLOSED_PATTERN))
            {
                CashBoxClosed = GetTagValue<int>(tag, CASHBOX_CLOSED_PATTERN);
            }
            else if (Regex.IsMatch(tag, CASH_BOX_AVAILABLE_PATTERN))
            {
                CashBoxAvailable = GetTagValue<int>(tag, CASH_BOX_AVAILABLE_PATTERN);
            }
            else if (Regex.IsMatch(tag, PEOPLE_INLINE_PATTERN))
            {
                PeopleInline = GetTagValue<int>(tag, PEOPLE_INLINE_PATTERN);
            }
            else if (Regex.IsMatch(tag, WAIT_TIME_PATTERN))
            {
                var waitTime = GetTagValue<string>(tag, WAIT_TIME_PATTERN);
                if (string.IsNullOrEmpty(waitTime))
                {
                    _waitTime = 0;
                }
                else
                {
                    _waitTime = RecognizeTime(waitTime);
                }
            }
            else if (!string.Equals(HATE_TO_WAIT_TAG, tag, StringComparison.OrdinalIgnoreCase))
            {
                Tags.Add(tag);
            }
        }

        #endregion
    }
}
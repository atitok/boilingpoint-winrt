﻿using System;

namespace BoilingPoint.Portable.Data.TweetEntities
{
    public class PhotoSize
    {
        public String Type { get; set; }

        public Int32 Width { get; set; }

        public Int32 Height { get; set; }
    }
}
﻿using System.Collections.Generic;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.Portable.Data
{
    public class TweetGrouping : List<Tweet>
    {
        private readonly Distance _radiusInKilometers;

        public TweetGrouping(double radiusInMeters)
        {
            _radiusInKilometers = Distance.ToKilometers(radiusInMeters);
        }

        public bool CanAdd(Tweet tweet)
        {
            if (Count == 0) return true;

            var geoPoint = new GeoPoint(this[0].Latitude, this[0].Longitude);
            Distance distance =
                geoPoint.DistanceBetween(new GeoPoint(tweet.Latitude, tweet.Longitude)).ToKilometers();
            return _radiusInKilometers >= distance;
        }
    }
}
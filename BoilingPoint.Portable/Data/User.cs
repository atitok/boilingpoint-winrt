﻿namespace BoilingPoint.Portable.Data
{
    public class User
    {
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string ProfileImageUrl { get; set; }

        public override string ToString()
        {
            return ScreenName;
        }

        protected bool Equals(User other)
        {
            return string.Equals(ScreenName, other.ScreenName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((User) obj);
        }

        public override int GetHashCode()
        {
            return (ScreenName != null ? ScreenName.GetHashCode() : 0);
        }
    }
}
using System;

namespace BoilingPoint.Portable
{
    /// <summary>
    ///     Describe either an Action (with optional userData argument) to be invoked
    ///     at the next possible time that is not right now.
    /// </summary>
    public class DeferredAction : IDeferredAction
    {
        #region Constructors

        /// <summary>
        ///     Construct a DeferredAction with no parameters
        /// </summary>
        /// <param name="callback">The callback to invoke</param>
        public DeferredAction(Action callback)
        {
            _callback = callback;
        }

        /// <summary>
        ///     Construct a DeferredAction with UserData argument
        /// </summary>
        /// <param name="callback">The callback to invoke</param>
        /// <param name="userData">The user data to pass as an argument</param>
        public DeferredAction(Action<object> callback, object userData)
        {
            _callbackParam = callback;
            _userData = userData;
        }

        #endregion

        #region Accessors

        /// <summary>
        ///     The System.Action compatible callback to invoke during the next frame.
        /// </summary>
        public Action Callback
        {
            get { return _callback; }
        }

        /// <summary>
        ///     The System.Action with an object UserData argument to invoke during the next frame.
        /// </summary>
        public Action<object> CallbackParam
        {
            get { return _callbackParam; }
        }

        /// <summary>
        ///     The UserData to provide to the valid CallbackParam action when invoked during the next frame.
        /// </summary>
        public object UserData
        {
            get { return _userData; }
        }

        #endregion

        #region IDeferredAction Members

        public virtual void Invoke()
        {
            if (_userData != null && _callbackParam == null)
                throw new ArgumentException("UserData specified but no CallbackParam to accept it");

            if (_callbackParam != null)
                CallbackParam.Invoke(_userData);
            else if (_callback != null)
                Callback.Invoke();
            else
                throw new Exception("No valid callback to invoke");
        }

        #endregion

        #region Private Member Data

        private readonly Action _callback;
        private readonly Action<object> _callbackParam;
        private readonly object _userData;

        #endregion
    }
}
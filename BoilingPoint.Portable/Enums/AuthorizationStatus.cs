﻿namespace BoilingPoint.Portable.Enums
{
    public enum AuthorizationStatus
    {
        Success,
        Failure,
        Denied
    }
}
﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;

namespace BoilingPoint.Portable.Events
{
    public class NotificationReceivedEventArgs : EventArgs
    {
        public NotificationType NotificationType { get; set; }
        public NotificationData NotificationData { get; set; }
    }
}
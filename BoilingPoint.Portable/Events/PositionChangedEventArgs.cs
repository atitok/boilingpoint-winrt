﻿using System;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.Portable.Events
{
    public class PositionChangedEventArgs : EventArgs
    {
        public GeoPoint Point { get; set; }
    }
}
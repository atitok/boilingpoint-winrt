﻿using System;
using System.Collections.Generic;
using BoilingPoint.Portable.Data;

namespace BoilingPoint.Portable.Events
{
    public class TweetsAddedEventArgs : EventArgs
    {
        public TweetsAddedEventArgs()
        {
            Tweets = new List<Tweet>();
        }

        public List<Tweet> Tweets { get; set; }
    }
}
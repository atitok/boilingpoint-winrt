﻿using System;

namespace BoilingPoint.Portable.Exceptions
{
    public class ConnectFailureException : Exception
    {
        public ConnectFailureException()
        {
        }

        public ConnectFailureException(string message)
            : base(message)
        {
        }

        public ConnectFailureException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
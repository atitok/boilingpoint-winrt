﻿using System;
using BoilingPoint.Portable.Geo.Google;

namespace BoilingPoint.Portable.Exceptions
{
    public class GoogleGeoCodingException : Exception
    {
        private const string defaultMessage =
            "There was an error processing the geocoding request. See Status or InnerException for more information.";

        public GoogleGeoCodingException(GoogleStatus status)
            : base(defaultMessage)
        {
            Status = status;
        }

        public GoogleGeoCodingException(Exception innerException)
            : base(defaultMessage, innerException)
        {
            Status = GoogleStatus.Error;
        }

        public GoogleStatus Status { get; private set; }
    }
}
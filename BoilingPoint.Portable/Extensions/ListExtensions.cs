﻿using System.Collections.Generic;

namespace BoilingPoint.Portable.Extensions
{
    public static class ListExtensions
    {
        public static void AddIfNotContains(this IList<string> list, string value)
        {
            if (!list.Contains(value))
            {
                list.Add(value);
            }
        }
    }
}
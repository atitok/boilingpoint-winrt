﻿namespace BoilingPoint.Portable.Geo
{
    public enum DistanceUnits
    {
        Miles,
        Kilometers,
        Meters
    }
}
﻿using System;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public abstract class BaseResponse
    {
        protected BaseResponse(JToken token)
        {
            ApiVersion = token.Value<string>("api_version");
            ResponseCode = (ResponseCode) Enum.ToObject(typeof (ResponseCode), token.Value<int>("response_code"));
            ErrorCode = token.Value<string>("error_code");
            ErrorMessage = token.Value<string>("error_message");
        }

        public string ApiVersion { get; set; }
        public ResponseCode ResponseCode { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class Contact
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "alias")]
        public string Alias { get; set; }

        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        public override string ToString()
        {
            return Alias ?? Value;
        }
    }
}
﻿namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public enum ContactType
    {
        Email,
        WebSite,
        Phone,
        Fax,
        Icq,
        Jabber,
        Skype
    }
}
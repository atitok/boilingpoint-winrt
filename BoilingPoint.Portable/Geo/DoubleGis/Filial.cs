﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class Filial
    {
        /// <summary>
        ///     Уникальный идентификатор филиала.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        ///     Долгота координаты места расположения филиала.
        /// </summary>
        [JsonProperty(PropertyName = "lon")]
        public double Longitude { get; set; }

        /// <summary>
        ///     Широта координаты места расположения филиала.
        /// </summary>
        [JsonProperty(PropertyName = "lat")]
        public double Latitude { get; set; }

        /// <summary>
        ///     Название филиала.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        ///     Расстояние в метрах от центра поиска до филиала.
        /// </summary>
        [JsonProperty(PropertyName = "dist")]
        public int Distance { get; set; }

        /// <summary>
        ///     Уникальный хэш, требуется для передачи в запрос на получение профиля филиала.
        /// </summary>
        [JsonProperty(PropertyName = "hash")]
        public string Hash { get; set; }

        /// <summary>
        ///     Город, к которому относится филиал.
        /// </summary>
        [JsonProperty(PropertyName = "city_name")]
        public string CityName { get; set; }

        /// <summary>
        ///     Адрес, по которому располагается филиал.
        /// </summary>
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        /// <summary>
        ///     Комментарий, иликраткое описание филиала.
        /// </summary>
        [JsonProperty(PropertyName = "micro_comment")]
        public string Comment { get; set; }

        /// <summary>
        ///     Список рубрик, в которые входит филиал.
        /// </summary>
        [JsonProperty(PropertyName = "rubrics")]
        public List<string> Rubrics { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
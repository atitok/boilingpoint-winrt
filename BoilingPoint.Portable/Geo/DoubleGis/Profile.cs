﻿using System;
using System.Collections.Generic;
using System.Linq;
using BoilingPoint.Portable.Data;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class Profile
    {
        #region Ctors

        public Profile()
        {
        }

        public Profile(JToken profileToken)
        {
            Id = profileToken.Value<string>("id");
            JToken firmGroupToken = profileToken.SelectToken("firm_group");
            if (firmGroupToken != null)
            {
                FirmId = firmGroupToken.Value<string>("id");
            }
            Longitude = profileToken.Value<double>("lon");
            Latitude = profileToken.Value<double>("lat");
            Name = profileToken.Value<string>("name");
            CityName = profileToken.Value<string>("city_name");
            Address = profileToken.Value<string>("address");

            Contacts = new List<Contact>();
            var contactsToken = profileToken.Value<JToken>("contacts");
            foreach (JToken contactToken in contactsToken)
            {
                var contactsContactToken = contactToken.Value<JToken>("contacts");
                foreach (JToken token in contactsContactToken)
                {
                    var contact = token.ToObject<Contact>();
                    Contacts.Add(contact);
                }
            }

            var scheduleToken = profileToken.Value<JToken>("schedule");
            if (scheduleToken != null)
            {
                Schedule = scheduleToken.ToObject<Schedule>();
            }
        }

        #endregion

        #region Properties

        public Bank Bank { get; set; }

        public Uri ImageUri { get; set; }

        public Filial Filial { get; set; }

        public int Distance
        {
            get { return Filial != null ? Filial.Distance : 0; }
        }

        /// <summary>
        ///     Время работы филиала.
        /// </summary>
        public Schedule Schedule { get; set; }

        /// <summary>
        ///     Уникальный идентификатор филиала.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///     Уникальный идентификатор фирмы.
        /// </summary>
        public string FirmId { get; set; }

        /// <summary>
        ///     Долгота координаты места расположения филиала.
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        ///     Широта координаты места расположения филиала.
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        ///     азвание филиала.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Город, к которому относится филиал.
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        ///     Адрес, по которому располагается филиал.
        /// </summary>
        public string Address { get; set; }

        public List<Contact> Contacts { get; set; }

        #endregion

        #region Methods

        public string GetWebSite()
        {
            return GetWebSite(this);
        }

        public static string GetWebSite(Profile profile)
        {
            Contact contact = profile.Contacts.FirstOrDefault(x => x.Type == "website");
            if (contact == null) return null;
            return contact.Alias ?? contact.Value;
        }

        public override string ToString()
        {
            return Name;
        }

        protected bool Equals(Profile other)
        {
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Profile) obj);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        #endregion
    }
}
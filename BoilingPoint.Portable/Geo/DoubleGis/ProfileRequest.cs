﻿namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class ProfileRequest : BaseRequest
    {
        public ProfileRequest(string id, string hash)
            : base("profile")
        {
            AddParameter("id", id);
            AddParameter("hash", hash);
        }
    }
}
﻿using Newtonsoft.Json;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class Schedule
    {
        [JsonProperty("Mon")]
        public ScheduleDay Monday { get; set; }

        [JsonProperty("Tue")]
        public ScheduleDay Tuesday { get; set; }

        [JsonProperty("Wed")]
        public ScheduleDay Wednesday { get; set; }

        [JsonProperty("Thu")]
        public ScheduleDay Thursday { get; set; }

        [JsonProperty("Fri")]
        public ScheduleDay Friday { get; set; }

        [JsonProperty("Sat")]
        public ScheduleDay Saturday { get; set; }

        [JsonProperty("Sun")]
        public ScheduleDay Sunday { get; set; }
    }
}
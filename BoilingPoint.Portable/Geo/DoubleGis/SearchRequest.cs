﻿using System.Globalization;
using BoilingPoint.Portable.Services;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class SearchRequest : BaseRequest
    {
        public SearchRequest(string what, GeoPoint geoPoint, int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS,
                             SortBy sortBy = SortBy.Relevance)
            : base("search")
        {
            AddParameter("what", what);
            AddParameter("point",
                         string.Format("{0},{1}",
                                       geoPoint.Longitude.ToString(CultureInfo.InvariantCulture),
                                       geoPoint.Latitude.ToString(CultureInfo.InvariantCulture)));
            AddParameter("radius", radiusInMeters);
            AddParameter("sort", sortBy.ToString().ToLower());
        }
    }
}
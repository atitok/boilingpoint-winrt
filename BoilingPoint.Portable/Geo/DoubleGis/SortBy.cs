﻿namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public enum SortBy
    {
        Relevance,
        Rating,
        Name,
        Distance
    }
}
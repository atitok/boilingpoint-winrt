﻿using Newtonsoft.Json;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class WorkingHours
    {
        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}", From, To);
        }
    }
}
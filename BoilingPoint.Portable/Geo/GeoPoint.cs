﻿using System;

namespace BoilingPoint.Portable.Geo
{
    public class GeoPoint
    {
        #region Ctors

        public GeoPoint()
        {
        }

        public GeoPoint(double latitude, double longitude)
        {
            if (longitude <= -180 || longitude > 180)
                throw new ArgumentOutOfRangeException("longitude", "Value must be between -180 and 180 (inclusive).");

            if (latitude < -90 || latitude > 90)
                throw new ArgumentOutOfRangeException("latitude",
                                                      "Value must be between -90(inclusive) and 90(inclusive).");

            if (double.IsNaN(longitude))
                throw new ArgumentException("Longitude must be a valid number.", "longitude");

            if (double.IsNaN(latitude))
                throw new ArgumentException("Latitude must be a valid number.", "latitude");

            Latitude = latitude;
            Longitude = longitude;
        }

        #endregion

        #region Properites

        public static GeoPoint Empty = new GeoPoint(0, 0);
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        #endregion

        #region Methods

        private double ToRadian(double val)
        {
            return (Math.PI/180.0)*val;
        }

        public Distance DistanceBetween(GeoPoint geoPoint)
        {
            return DistanceBetween(geoPoint, DistanceUnits.Miles);
        }

        public Distance DistanceBetween(GeoPoint geoPoint, DistanceUnits units)
        {
            double earthRadius = (units == DistanceUnits.Miles)
                                     ? Distance.EarthRadiusInMiles
                                     : Distance.EarthRadiusInKilometers;

            double latRadian = ToRadian(geoPoint.Latitude - Latitude);
            double longRadian = ToRadian(geoPoint.Longitude - Longitude);

            double a = Math.Pow(Math.Sin(latRadian/2.0), 2) +
                       Math.Cos(ToRadian(Latitude))*
                       Math.Cos(ToRadian(geoPoint.Latitude))*
                       Math.Pow(Math.Sin(longRadian/2.0), 2);

            double c = 2.0*Math.Asin(Math.Min(1, Math.Sqrt(a)));

            double distance = earthRadius*c;
            return new Distance(distance, units);
        }

        protected bool Equals(GeoPoint other)
        {
            return Latitude.Equals(other.Latitude) && Longitude.Equals(other.Longitude);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((GeoPoint) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Latitude.GetHashCode()*397) ^ Longitude.GetHashCode();
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}", Latitude, Longitude);
        }

        #endregion
    }
}
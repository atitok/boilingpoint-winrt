﻿namespace BoilingPoint.Portable.Geo.Google
{
    public abstract class BaseGoogleService
    {
        /// <remarks>
        ///     http://code.google.com/apis/maps/documentation/geocoding/#StatusCodes
        /// </remarks>
        protected GoogleStatus EvaluateStatus(string status)
        {
            switch (status)
            {
                case "OK":
                    return GoogleStatus.Ok;
                case "ZERO_RESULTS":
                    return GoogleStatus.ZeroResults;
                case "OVER_QUERY_LIMIT":
                    return GoogleStatus.OverQueryLimit;
                case "REQUEST_DENIED":
                    return GoogleStatus.RequestDenied;
                case "INVALID_REQUEST":
                    return GoogleStatus.InvalidRequest;
                default:
                    return GoogleStatus.Error;
            }
        }
    }
}
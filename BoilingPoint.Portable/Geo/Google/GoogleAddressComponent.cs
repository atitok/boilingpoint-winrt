﻿using System;

namespace BoilingPoint.Portable.Geo.Google
{
    public class GoogleAddressComponent
    {
        public GoogleAddressComponent(GoogleAddressType[] types, string longName, string shortName)
        {
            if (types == null)
                throw new ArgumentNullException("types");

            if (types.Length < 1)
                throw new ArgumentException("Value cannot be empty.", "types");

            Types = types;
            LongName = longName;
            ShortName = shortName;
        }

        public GoogleAddressType[] Types { get; private set; }
        public string LongName { get; private set; }
        public string ShortName { get; private set; }

        public override string ToString()
        {
            return String.Format("{0}: {1}", Types[0], LongName);
        }
    }
}
namespace BoilingPoint.Portable.Geo.Google
{
    public enum GoogleStatus
    {
        Error,
        Ok,
        ZeroResults,
        OverQueryLimit,
        RequestDenied,
        InvalidRequest
    }
}
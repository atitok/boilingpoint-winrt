﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoilingPoint.Portable.Geo
{
    public interface IGeocoder
    {
        Task<IList<GeoAddress>> GeocodeAsync(string address, string lang = "", string region = "");
        Task<IList<GeoAddress>> ReverseGeocodeAsync(GeoPoint geoPoint, string lang = "");
        Task<IList<GeoAddress>> ReverseGeocodeAsync(double latitude, double longitude, string lang = "");
        Task<string> GetCountryCodeAsync(double latitude, double longitude);
    }
}
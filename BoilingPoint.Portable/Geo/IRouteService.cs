﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BoilingPoint.Portable.Geo
{
    public interface IRouteService
    {
        Task<IList<GeoPoint>> CalculateRouteAsync(GeoPoint origin, GeoPoint destination);
    }
}
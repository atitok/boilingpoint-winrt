﻿using System;

namespace BoilingPoint.Portable.Geo.Yandex
{
    public class CountryDetails
    {
        public String Name { get; set; }

        public String NameCode { get; set; }

        public String AddressLine { get; set; }
    }
}
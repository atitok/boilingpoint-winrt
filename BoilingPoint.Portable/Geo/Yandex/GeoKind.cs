﻿namespace BoilingPoint.Portable.Geo.Yandex
{
    /// <summary>
    ///     Geo kind.
    /// </summary>
    /// <remarks>
    ///     http://api.yandex.ru/maps/doc/geocoder/desc/reference/kind.xml
    /// </remarks>
    public enum GeoKind
    {
        Other, // Разное.
        House, // Отдельный дом.
        Street, // Улица.
        Metro, // Станция метро.
        District, // Район города.
        Locality, // Населённый пункт: город/поселок/деревня/село/....
        Area, // Район области.
        Province, // Область.
        Country, // Страна.
        Hydro, // Река,озеро,ручей,водохранилище....
        Railway, // Ж.д. станция.
        Route, // Линия метро / шоссе / ж.д. линия.
        Vegetation, // Лес, парк....
        Cemetery, // Кладбище.
        Bridge, // Мост.
        Km, // Километр шоссе.
        Airport, // Аэропорт.
    }
}
﻿namespace BoilingPoint.Portable.Geo.Yandex
{
    /// <summary>
    ///     Geo Precision.
    /// </summary>
    public enum Precision
    {
        Other, // Улица не найдена, но найден, например, посёлок, район, и т. д.
        Exact, // Точное соответствие.
        Number, // Совпадает только номер дома.
        Near, // Найден дом поблизости (так как 18–16 < 10).
        Street, // Найдена улица (так как 18–4 > 10).
    }
}
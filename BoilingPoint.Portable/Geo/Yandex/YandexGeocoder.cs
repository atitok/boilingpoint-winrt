using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.Yandex
{
    /// <summary>
    ///     Yandex Geocoder.
    /// </summary>
    public class YandexGeocoder : IYandexGeocoder
    {
        #region Fields

        private const String GEOCODER_URI = "http://geocode-maps.yandex.ru/1.x/?geocode={0}&format=json";

        #endregion Fields

        #region Methods

        public async Task<IList<GeoAddress>> GeocodeAsync(String address, String lang = "", String region = "")
        {
            String requestUri = String.Format(GEOCODER_URI, address);

            if (!String.IsNullOrEmpty(lang))
            {
                requestUri = String.Format("{0}&lang={1}", requestUri, lang);

                if (!String.IsNullOrEmpty(region))
                {
                    requestUri = String.Format("{0}-{1}", requestUri, region);
                }
            }

            return await HandleRequest(requestUri);
        }

        public async Task<IList<GeoAddress>> ReverseGeocodeAsync(GeoPoint geoPoint, String lang = "")
        {
            return await ReverseGeocodeAsync(geoPoint.Latitude, geoPoint.Longitude, lang);
        }

        public async Task<IList<GeoAddress>> ReverseGeocodeAsync(Double latitude, Double longitude, String lang = "")
        {
            String requestUri = String.Format(GEOCODER_URI, String.Format("{0},{1}",
                                                                          longitude.ToString(
                                                                              CultureInfo.InvariantCulture),
                                                                          latitude.ToString(CultureInfo.InvariantCulture)));

            if (!String.IsNullOrEmpty(lang))
            {
                requestUri = String.Format("{0}&lang={1}", requestUri, lang);
            }

            return await HandleRequest(requestUri);
        }

        public async Task<String> GetCountryCodeAsync(Double latitude, Double longitude)
        {
            String countryCode = null;

            IList<GeoAddress> addresses = await ReverseGeocodeAsync(latitude, longitude);

            YandexAddress yaAddress = addresses.Cast<YandexAddress>().FirstOrDefault();
            if (yaAddress != null && yaAddress.Country != null)
            {
                countryCode = yaAddress.Country.NameCode;
            }

            return countryCode;
        }

        private async Task<List<GeoAddress>> HandleRequest(String uri)
        {
            List<GeoAddress> addresses;

            try
            {
                using (var client = new HttpClient())
                {
                    String response = await client.GetStringAsync(uri);
                    addresses = ParseAddresses(response);
                }
            }
            catch (WebException)
            {
                addresses = new List<GeoAddress>();
            }

            return addresses;
        }

        private List<GeoAddress> ParseAddresses(String serviceResponse)
        {
            var addresses = new List<GeoAddress>();

            JObject jObject = JObject.Parse(serviceResponse);

            var geoItems = (JArray) jObject.SelectToken("response.GeoObjectCollection.featureMember");

            foreach (JToken token in geoItems)
            {
                addresses.Add(ParseAddress(token));
            }

            return addresses;
        }

        private GeoAddress ParseAddress(JToken token)
        {
            token = token["GeoObject"];

            JToken pointToken = token.SelectToken("Point.pos");
            JToken metadataToken = token.SelectToken("metaDataProperty.GeocoderMetaData");
            JToken formattedAddressToken = metadataToken["text"];
            JToken kindToken = metadataToken["kind"];
            JToken precisionToken = metadataToken["precision"];

            var kind = (GeoKind) Enum.Parse(typeof (GeoKind), kindToken.Value<String>(), true);
            var precision = (Precision) Enum.Parse(typeof (Precision), precisionToken.Value<String>(), true);
            GeoPoint point = ParseGeoPoint(pointToken);

            var address = new YandexAddress(kind, formattedAddressToken.ToString(), point, precision)
                              {
                                  Country = ParseCountry(metadataToken)
                              };

            // TODO: Later we can implement parsing AdministrativeArea, Locality, Thoroughfare.

            return address;
        }

        private GeoPoint ParseGeoPoint(JToken token)
        {
            Double lat = 0;
            Double lon = 0;

            Match pointMatch = Regex.Match(token.ToString(), @"(?<lon>\d+\.\d+) (?<lat>\d+\.\d+)");

            if (!Double.TryParse(pointMatch.Groups["lat"].Value, NumberStyles.AllowDecimalPoint,
                                 CultureInfo.InvariantCulture, out lat)
                ||
                !Double.TryParse(pointMatch.Groups["lon"].Value, NumberStyles.AllowDecimalPoint,
                                 CultureInfo.InvariantCulture, out lon))
            {
                throw new InvalidOperationException("Bad values in the service response");
            }

            var geoPoint = new GeoPoint(lat, lon);

            return geoPoint;
        }

        /// <summary>
        ///     Parses the country.
        /// </summary>
        /// <param name="token">The metadata token.</param>
        /// <returns>Country details.</returns>
        private CountryDetails ParseCountry(JToken token)
        {
            token = token.SelectToken("AddressDetails.Country");

            var country = new CountryDetails
                              {
                                  AddressLine = (String) token["AddressLine"],
                                  Name = (String) token["CountryName"],
                                  NameCode = (String) token["CountryNameCode"]
                              };

            return country;
        }

        #endregion Methods
    }
}
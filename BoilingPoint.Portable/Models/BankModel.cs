﻿using BoilingPoint.Portable.Data;

namespace BoilingPoint.Portable.Models
{
    public class BankModel : PropertyChangedBase
    {
        public BankModel(Bank bank)
        {
            Title = bank.Title;
            TwitterUsername = bank.TwitterUsername;
        }

        public string TwitterUsername { get; set; }

        public string Title { get; set; }
    }
}
﻿using System;

using BoilingPoint.Portable.Geo.DoubleGis;

namespace BoilingPoint.Portable.Models
{
    using Geo;

    public class FilialModel
    {
        #region Constructors

        public FilialModel(Filial filial)
        {
            this.Id = filial.Id;
            this.Name = filial.Name;
            this.Comment = filial.Comment;
            this.Rubrics = String.Join(", ", filial.Rubrics);
            this.Distance = filial.Distance;
            this.Latitude = filial.Latitude;
            this.Longitude = filial.Longitude;

            this.Address = String.Format("{0}, {1}", filial.CityName, filial.Address);
        }

        #endregion Constructors

        #region Properties

        public String Id { get; set; }

        public String Name { get; set; }

        public Int32 Distance { get; set; }

        public String DistanceFormatted
        {
            get
            {
                String val = null;

                if (this.Distance < Geo.Distance.MetersInKilometers)
                {
                    val = string.Format("{0} м.", this.Distance);
                }
                else
                {
                    val = string.Format("{0} км.", Geo.Distance.ToKilometers(this.Distance).Value);
                }

                return val;
            }
        }

        public String Address { get; set; }

        public String Comment { get; set; }

        public String Rubrics { get; set; }

        public Double Latitude { get; set; }

        public Double Longitude { get; set; }

        public GeoPoint Location
        {
            get {return new GeoPoint(this.Latitude, this.Longitude);}
        }

        #endregion Properties
    }
}

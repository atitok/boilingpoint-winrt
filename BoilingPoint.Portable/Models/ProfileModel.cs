﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo.DoubleGis;

namespace BoilingPoint.Portable.Models
{
    public class ProfileModel : PropertyChangedBase
    {
        #region Events

        public event EventHandler ContextMenuOpening;

        public void OnContextMenuOpening(object sender, EventArgs e)
        {
            if (ContextMenuOpening != null)
                ContextMenuOpening(sender, e);
        }

        public event EventHandler ShowOnMapTap;

        protected void OnShowOnMapTap(object sender, EventArgs e)
        {
            if (ShowOnMapTap != null)
                ShowOnMapTap(sender, e);
        }

        public event EventHandler ShowTweetsInFilialTap;

        protected void OnShowTweetsInFilialTap(object sender, EventArgs e)
        {
            if (ShowTweetsInFilialTap != null)
                ShowTweetsInFilialTap(sender, e);
        }

        public event EventHandler ShowTweetsInBankTap;

        protected void OnShowTweetsInBankTap(object sender, EventArgs e)
        {
            if (ShowTweetsInBankTap != null)
                ShowTweetsInBankTap(sender, e);
        }

        #endregion

        #region Fields

        private Bank _bank;

        #endregion

        #region Ctors

        public ProfileModel()
        {
        }

        public ProfileModel(Profile profile)
        {
            Id = profile.Id;
            Name = profile.Name;
            Address = profile.Address;
            Distance = profile.Distance;

            if (profile.Distance < Geo.Distance.MetersInKilometers)
            {
                DistanceFormatted = string.Format("{0} м.", profile.Distance);
            }
            else
            {
                DistanceFormatted = string.Format("{0} км.",
                                                  Geo.Distance.ToKilometers(profile.Distance).Value);
            }
        }

        public int Distance { get; set; }

        #endregion

        #region Properties

        public Bank Bank
        {
            get { return _bank; }
            set
            {
                if (Equals(value, _bank)) return;
                _bank = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("BankId");
                NotifyOfPropertyChange("BankVisible");
            }
        }

        public string BankId
        {
            get { return Bank != null ? Bank.objectId : null; }
        }

        public bool BankVisible
        {
            get { return Bank != null; }
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string DistanceFormatted { get; set; }

        #endregion

        #region Methods

        public void OnShowOnMapTap()
        {
            OnShowOnMapTap(this, EventArgs.Empty);
        }

        public void OnShowTweetsInFilialTap()
        {
            OnShowTweetsInFilialTap(this, EventArgs.Empty);
        }

        public void OnShowTweetsInBankTap()
        {
            OnShowTweetsInBankTap(this, EventArgs.Empty);
        }

        protected bool Equals(ProfileModel other)
        {
            return string.Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ProfileModel) obj);
        }

        public override int GetHashCode()
        {
            return (Id != null ? Id.GetHashCode() : 0);
        }

        #endregion
    }
}
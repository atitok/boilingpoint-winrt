﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BoilingPoint.Portable.PhoneNumbers
{
    public static class PhoneNumberUtil
    {
        // We use this pattern to check if the phone number has at least three letters in it - if so, then
        // we treat it as a number where some phone-number digits are represented by letters.
        private static readonly PhoneRegex VALID_ALPHA_PHONE_PATTERN =
            new PhoneRegex("(?:.*?[A-Za-z]){3}.*", RegexOptions.None);

        // For performance reasons, amalgamate both into one map.
        private static readonly Dictionary<char, char> ALPHA_PHONE_MAPPINGS;

        // Only upper-case variants of alpha characters are stored.
        private static readonly Dictionary<char, char> ALPHA_MAPPINGS;

        static PhoneNumberUtil()
        {
            // Simple ASCII digits map used to populate ALPHA_PHONE_MAPPINGS and
            // ALL_PLUS_NUMBER_GROUPING_SYMBOLS.
            var asciiDigitMappings = new Dictionary<char, char>
                                         {
                                             {'0', '0'},
                                             {'1', '1'},
                                             {'2', '2'},
                                             {'3', '3'},
                                             {'4', '4'},
                                             {'5', '5'},
                                             {'6', '6'},
                                             {'7', '7'},
                                             {'8', '8'},
                                             {'9', '9'},
                                         };

            var alphaMap = new Dictionary<char, char>();
            alphaMap['A'] = '2';
            alphaMap['B'] = '2';
            alphaMap['C'] = '2';
            alphaMap['D'] = '3';
            alphaMap['E'] = '3';
            alphaMap['F'] = '3';
            alphaMap['G'] = '4';
            alphaMap['H'] = '4';
            alphaMap['I'] = '4';
            alphaMap['J'] = '5';
            alphaMap['K'] = '5';
            alphaMap['L'] = '5';
            alphaMap['M'] = '6';
            alphaMap['N'] = '6';
            alphaMap['O'] = '6';
            alphaMap['P'] = '7';
            alphaMap['Q'] = '7';
            alphaMap['R'] = '7';
            alphaMap['S'] = '7';
            alphaMap['T'] = '8';
            alphaMap['U'] = '8';
            alphaMap['V'] = '8';
            alphaMap['W'] = '9';
            alphaMap['X'] = '9';
            alphaMap['Y'] = '9';
            alphaMap['Z'] = '9';

            ALPHA_MAPPINGS = alphaMap;

            var combinedMap = new Dictionary<char, char>(ALPHA_MAPPINGS);
            foreach (var k in asciiDigitMappings)
                combinedMap[k.Key] = k.Value;

            ALPHA_PHONE_MAPPINGS = combinedMap;
        }


        /* Normalizes a string of characters representing a phone number by replacing all characters found
        * in the accompanying map with the values therein, and stripping all other characters if
        * removeNonMatches is true.
        *
        * @param number                     a string of characters representing a phone number
        * @param normalizationReplacements  a mapping of characters to what they should be replaced by in
        *                                   the normalized version of the phone number
        * @param removeNonMatches           indicates whether characters that are not able to be replaced
        *                                   should be stripped from the number. If this is false, they
        *                                   will be left unchanged in the number.
        * @return  the normalized string version of the phone number
        */

        private static String NormalizeHelper(String number, Dictionary<char, char> normalizationReplacements,
                                              bool removeNonMatches)
        {
            var normalizedNumber = new StringBuilder(number.Length);
            char[] numberAsCharArray = number.ToCharArray();
            foreach (char character in numberAsCharArray)
            {
                char newDigit;
                if (normalizationReplacements.TryGetValue(char.ToUpper(character), out newDigit))
                    normalizedNumber.Append(newDigit);
                else if (!removeNonMatches)
                    normalizedNumber.Append(character);
                // If neither of the above are true, we remove this character.
            }
            return normalizedNumber.ToString();
        }

        /**
        * Normalizes a string of characters representing a phone number. This performs the following
        * conversions:
        *   Punctuation is stripped.
        *   For ALPHA/VANITY numbers:
        *   Letters are converted to their numeric representation on a telephone keypad. The keypad
        *       used here is the one defined in ITU Recommendation E.161. This is only done if there are
        *       3 or more letters in the number, to lessen the risk that such letters are typos.
        *   For other numbers:
        *   Wide-ascii digits are converted to normal ASCII (European) digits.
        *   Arabic-Indic numerals are converted to European numerals.
        *   Spurious alpha characters are stripped.
        *   Arabic-Indic numerals are converted to European numerals.
        *
        * @param number  a string of characters representing a phone number
        * @return        the normalized string version of the phone number
        */

        public static String Normalize(String number)
        {
            if (VALID_ALPHA_PHONE_PATTERN.MatchAll(number).Success)
                return NormalizeHelper(number, ALPHA_PHONE_MAPPINGS, true);
            return NormalizeDigitsOnly(number);
        }

        /**
        * Normalizes a string of characters representing a phone number. This converts wide-ascii and
        * arabic-indic numerals to European numerals, and strips punctuation and alpha characters.
        *
        * @param number  a string of characters representing a phone number
        * @return        the normalized string version of the phone number
        */

        public static String NormalizeDigitsOnly(String number)
        {
            return NormalizeDigits(number, false /* strip non-digits */).ToString();
        }

        internal static StringBuilder NormalizeDigits(String number, bool keepNonDigits)
        {
            var normalizedDigits = new StringBuilder(number.Length);
            foreach (char c in number)
            {
                var digit = (int) char.GetNumericValue(c);
                if (digit != -1)
                {
                    normalizedDigits.Append(digit);
                }
                else if (keepNonDigits)
                {
                    normalizedDigits.Append(c);
                }
            }
            return normalizedDigits;
        }
    }
}
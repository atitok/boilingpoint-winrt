﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Services
{
    public class AnonymousTwitterService : TwitterService, IAnonymousTwitterService
    {
        #region Events

        public event EventHandler<TweetsAddedEventArgs> TweetsAdded;

        protected virtual void OnTweetsAdded(object sender, TweetsAddedEventArgs e)
        {
            if (TweetsAdded != null)
                TweetsAdded(sender, e);
        }

        #endregion

        #region Fields

        private readonly List<Tweet> _tweets;

        #endregion

        #region Ctors

        public AnonymousTwitterService(
            IGeocoder geocoder,
            IDataService dataService)
            : base(geocoder, dataService)
        {
            _tweets = new List<Tweet>();
        }

        #endregion

        #region Properties

        public override bool IsAuthorized
        {
            get { return true; }
        }

        public List<Tweet> Tweets
        {
            get { return _tweets; }
        }

        #endregion

        #region Methods

        public async Task<List<User>> GetUsersAsync(string userScreenNames)
        {
            var users = new List<User>();
            if (!string.IsNullOrEmpty(userScreenNames))
            {
                try
                {
                    Config config = await _dataService.GetConfigAsync();

                    string s;
                    using (var httpClient = new HttpClient())
                    {
                        var requestUri =
                            new Uri(string.Format("{0}twitter/users?userScreenNames={1}",
                                                  config.AppBaseUrl,
                                                  userScreenNames));
                        using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri))
                        {
                            using (HttpResponseMessage responseMessage = await httpClient.SendAsync(requestMessage))
                            {
                                s = await responseMessage.Content.ReadAsStringAsync();
                            }
                        }
                    }

                    if (s != null)
                    {
                        users = JToken.Parse(s).ToObject<List<User>>();
                    }
                }
                catch (Exception)
                {
                    users = new List<User>();
                }
            }

            return users;
        }

        public override async Task<List<Tweet>>
            SearchAsync(string query, double latitude, double longitude, int radiusInMeters, ulong sinceID = 0,
                        ulong maxID = 0)
        {
            GeoPoint geoPoint = null;
            if (!Equals(latitude, 0d) && !Equals(longitude, 0d))
            {
                geoPoint = new GeoPoint(latitude, longitude);
            }

            List<Tweet> tweets =
                (await _dataService.GetTweetsAsync(geoPoint, radiusInMeters, sinceID, maxID))
                    .OrderByDescending(tweet => tweet.CreatedAtTimestamp).ToList();

            var e = new TweetsAddedEventArgs();
            foreach (Tweet tweet in tweets)
            {
                if (_tweets.Contains(tweet)) continue;
                _tweets.Add(tweet);
                e.Tweets.Add(tweet);
            }

            if (e.Tweets.Count > 0)
            {
                OnTweetsAdded(this, e);
            }

            return tweets;
        }

        public override Task<Tweet> SendTweetAsync(Tweet tweet)
        {
            throw new NotImplementedException();
        }

        public override Task AuthorizeAsync()
        {
            var completionSource = new TaskCompletionSource<bool>();
            completionSource.TrySetResult(IsAuthorized);
            return completionSource.Task;
        }

        #endregion
    }
}
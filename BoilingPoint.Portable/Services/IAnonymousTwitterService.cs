﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;

namespace BoilingPoint.Portable.Services
{
    public interface IAnonymousTwitterService
    {
        List<Tweet> Tweets { get; }
        event EventHandler<TweetsAddedEventArgs> TweetsAdded;

        Task<List<User>> GetUsersAsync(string userScreenNames);

        Task<List<Tweet>>
            SearchAsync(
            string query,
            double latitude = 0,
            double longitude = 0,
            int radiusInMeters = 0,
            ulong sinceID = 0,
            ulong maxID = 0);

        Task<List<Tweet>>
            SearchAsync(
            string query,
            ulong sinceID = 0,
            ulong maxID = 0);
    }
}
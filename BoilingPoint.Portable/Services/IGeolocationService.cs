﻿using System;
using System.Threading.Tasks;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.Portable.Services
{
    public interface IGeolocationService
    {
        GeolocationStatus Status { get; }
        bool Started { get; }
        event EventHandler<GeolocationStatusChangedEventArgs> StatusChanged;
        event EventHandler<PositionChangedEventArgs> PositionChanged;
        Task<GeoPoint> GetGeoPointAsync();
        void Start();
        void Stop();
    }
}
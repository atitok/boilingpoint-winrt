﻿using System;
using System.Collections.Generic;

namespace BoilingPoint.Portable.Services
{
    public interface ILogService
    {
        void Debug(string format, params object[] args);
        void Debug(object value);
        void Error(Exception ex, Dictionary<string, string> logExtra = null);
    }
}
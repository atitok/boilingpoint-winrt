﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;

namespace BoilingPoint.Portable.Services
{
    public interface INotificationService
    {
        event EventHandler DeviceRegistered;
        event EventHandler<NotificationReceivedEventArgs> NotificationReceived;

        void RegisterDevice(Device device);
    }
}
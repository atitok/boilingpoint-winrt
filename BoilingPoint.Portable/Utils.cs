﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BoilingPoint.Portable
{
    public static class Utils
    {
        private static readonly Dictionary<string, string> _countryCodes =
            new Dictionary<string, string>
                {
                    {"Россия", "ru"},
                    {"Азербайджан", "az"},
                    {"Армения", "am"},
                    {"Беларусь", "by"},
                    {"Грузия", "ge"},
                    {"Казахстан", "kz"},
                    {"Кыргызстан", "kg"},
                    {"Латвия", "lv"},
                    {"Литва", "lt"},
                    {"Молдавия", "md"},
                    {"Таджикистан", "tj"},
                    {"Туркменистан", "tm"},
                    {"Узбекистан", "uz"},
                    {"Украина", "ua"},
                    {"Эстония", "ee"},
                    {"Польша", "pl"}
                };

        public static string Plural(int n, string[] forms)
        {
            int plural = (n%10 == 1 && n%100 != 11 ? 0 : n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20) ? 1 : 2);
            return forms.Length > plural ? forms[plural] : string.Empty;
        }

        public static string GetCountryCode(string country)
        {
            string countryCode = null;
            if (_countryCodes.ContainsKey(country))
            {
                countryCode = _countryCodes[country];
            }
            return countryCode;
        }

        public static void ReplaceIgnoreCase(StringBuilder sb, string oldValue, string newValue)
        {
            Match match = Regex.Match(sb.ToString(), oldValue, RegexOptions.IgnoreCase);
            if (match.Success)
            {
                sb.Remove(match.Index, oldValue.Length).Insert(match.Index, newValue);
            }
        }

        public static string GetLowerCaseHashtag(string s)
        {
            var builder = new StringBuilder();
            if (s != null)
            {
                foreach (char c in from char c in s where char.IsLetterOrDigit(c) select c)
                {
                    builder.Append(c);
                }
            }
            return builder.ToString().ToLower();
        }

        public static string GetTitleCaseHashtag(string s)
        {
            bool b = false;
            var builder = new StringBuilder();
            if (s != null)
            {
                foreach (char c in s)
                {
                    if (char.IsLetterOrDigit(c))
                    {
                        if (b)
                        {
                            builder.Append(char.ToUpper(c));
                            b = false;
                        }
                        else
                        {
                            builder.Append(c);
                        }
                    }
                    else
                    {
                        b = true;
                    }
                }
            }
            return builder.ToString();
        }

        public static string RemoveParenthesis(string s)
        {
            int index = s.IndexOf('(');
            if (index == -1) return s.Trim();
            int lastIndex = s.LastIndexOf(')');
            s = s.Remove(index, lastIndex - index + 1);
            return s.Trim();
        }

        public static string NormalizeWebSite(string webSite)
        {
            if (webSite != null)
            {
                if (!webSite.StartsWith("http"))
                    webSite = string.Format("http://{0}", webSite);

                Uri uri;
                if (!Uri.TryCreate(webSite, UriKind.Absolute, out uri))
                    return string.Empty;

                webSite = uri.Host.Replace("www.", string.Empty);
            }
            return webSite;
        }

        public static DateTime UnixTimeStampToDateTime(int unixTimeStamp)
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp);
            return dateTime;
        }

        public static int DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (int) (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;
using BugSense;
using Caliburn.Micro;
using Parse;

using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using BoilingPoint.WinRT.Services;
using BoilingPoint.WinRT.ViewModels;
using BoilingPoint.WinRT.Views;
using DataService = BoilingPoint.WinRT.Services.DataService;
using BoilingPoint.Portable.Geo.Yandex;
using TwitterService = BoilingPoint.WinRT.Services.TwitterService;

// The Grid App template is documented at http://go.microsoft.com/fwlink/?LinkId=234226

namespace BoilingPoint.WinRT
{
    /// <summary>
    ///     Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App
    {
        #region Fields

        private WinRTContainer _container;

        #endregion

        /// <summary>
        ///     Initializes the singleton Application Object.  This is the first line of authored code
        ///     executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            InitializeComponent();

            Suspending += OnSuspending;
        }

        public static String GetResource(String name)
        {
            ResourceLoader resourceLoader = new ResourceLoader();

            return resourceLoader.GetString(name);
        }

        protected override void Configure()
        {
            this._container = new WinRTContainer();
            this._container.RegisterWinRTServices();

            this._container.PerRequest<MainViewModel>();
            this._container.PerRequest<ProfileViewModel>();
            this._container.PerRequest<MapViewModel>();

            this._container.RegisterSingleton(typeof(IGeocoder), null, typeof(YandexGeocoder));
            this._container.RegisterSingleton(typeof(ILogService), null, typeof(LogService));
            this._container.RegisterSingleton(typeof(ITwitterService), null, typeof(TwitterService));
            this._container.RegisterSingleton(typeof(IDataService), null, typeof(DataService));
            this._container.RegisterSingleton(typeof(IGeolocationService), null, typeof(GeolocationService));
            this._container.RegisterSingleton(typeof(INotificationService), null, typeof(NotificationService));
            this._container.RegisterSingleton(typeof(IAnonymousTwitterService), null, typeof(AnonymousTwitterService));

            ParseClient.Initialize(Settings.PARSE_APPLICATION_ID, Settings.PARSE_WINDOWS_KEY);
            BugSenseHandler.Instance.Init(this, Settings.BUGSENSE_API_KEY);
		}

        protected override void OnUnhandledException(Object sender, Windows.UI.Xaml.UnhandledExceptionEventArgs e)
        {
            var logService = (ILogService) this._container.GetInstance(typeof (ILogService), null);
            if (logService != null)
            {
                logService.Error(e.Exception);
            }

            if (Debugger.IsAttached)
            {
                Debugger.Break();
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }

            base.OnUnhandledException(sender, e);
        }

        protected override Object GetInstance(Type service, String key)
        {
            return this._container.GetInstance(service, key);
        }

        protected override IEnumerable<Object> GetAllInstances(Type service)
        {
            return this._container.GetAllInstances(service);
        }

        protected override void BuildUp(Object instance)
        {
            this._container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            this._container.RegisterNavigationService(rootFrame);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (Equals(args.TileId, "App"))
            {
                DisplayRootView<MainView>();
            }
        }
    }
}
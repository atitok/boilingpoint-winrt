﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace BoilingPoint.WinRT.Controls
{
    public sealed partial class MultiselectComboBox : UserControl
    {
        public MultiselectComboBox()
        {
            this.InitializeComponent();

            this.ComboBox.ItemsSource = new[] { "one", "two", "three" };
        }

        public IList<Object> SelectedItems { get; private set; }
    }
}

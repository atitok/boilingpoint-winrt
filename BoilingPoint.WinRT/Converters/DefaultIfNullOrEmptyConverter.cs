﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace BoilingPoint.WinRT.Converters
{
    public class DefaultIfNullOrEmptyConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object parameter, String language)
        {
            String stringVal = value as String;
            if (String.IsNullOrEmpty(stringVal))
            {
                return parameter;
            }

            return value;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, String language)
        {
            return value;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using PositionChangedEventArgs = Windows.Devices.Geolocation.PositionChangedEventArgs;

using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;

namespace BoilingPoint.WinRT.Services
{
    public class GeolocationService : BaseGeolocationService
    {
        #region Fields

        private Geolocator _locator = null;

        private GeolocationStatus _status = GeolocationStatus.Disabled;

        #endregion Fields

        #region Constructors

        public GeolocationService()
        {
            this.Init();
        }

        #endregion Constructors

        #region Properties

        public override GeolocationStatus Status
        {
            get { return this._status; }
        }

        public override bool Started
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        public override async Task<GeoPoint> GetGeoPointAsync()
        {
            GeoPoint geoPoint = null;
            
            Geoposition geoposition = await this._locator.GetGeopositionAsync();
            if (geoposition != null)
            {
                double latitude = geoposition.Coordinate.Latitude;
                double longitude = geoposition.Coordinate.Longitude;
                geoPoint = new GeoPoint(latitude, longitude);
            }
            return geoPoint;
        }

        public override void Start()
        {
        }

        public override void Stop()
        {
        }

        private void Init()
        {
            this._locator = new Geolocator();

            this._locator.PositionChanged += OnLocatorPositionChanged;
            this._locator.StatusChanged += OnLocatorStatusChanged;
        }

        private void OnLocatorPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            GeoPoint point = new GeoPoint(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);

            Portable.Events.PositionChangedEventArgs e = new Portable.Events.PositionChangedEventArgs()
            {
                Point = point
            };

            this.OnPositionChanged(sender, e);
        }

        private void OnLocatorStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            this._status =
                (GeolocationStatus) Enum.Parse(typeof (GeolocationStatus), args.Status.ToString());

            GeolocationStatusChangedEventArgs e = new GeolocationStatusChangedEventArgs()
            {
                Status = _status
            };

            this.OnStatusChanged(sender, e);
        }

        #endregion Methods
    }
}
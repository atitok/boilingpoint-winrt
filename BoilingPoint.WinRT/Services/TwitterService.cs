﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Data.TweetEntities;
using BoilingPoint.Portable.Exceptions;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using LinqToTwitter;
using Newtonsoft.Json.Linq;
using PhotoSize = BoilingPoint.Portable.Data.TweetEntities.PhotoSize;
using User = BoilingPoint.Portable.Data.User;

namespace BoilingPoint.WinRT.Services
{
    /// <summary>
    ///     Twitter service.
    /// </summary>
    public class TwitterService : Portable.Services.TwitterService
    {
        #region Fields

        private WinRtAuthorizer _authorizer;

        #endregion Fields

        #region Methods

        public TwitterService(IGeocoder geocoder, IDataService dataService)
            : base(geocoder, dataService)
        {
        }

        public override bool IsAuthorized
        {
            get { return _authorizer != null && _authorizer.IsAuthorized; }
        }

        public override async Task<Tweet> SendTweetAsync(Tweet tweet)
        {
            Guard.ArgumentIsNotNull(tweet, "tweet");

            await CheckAuthorizedAsync();

            var completionSource = new TaskCompletionSource<Tweet>();
            if (IsAuthorized)
            {
                using (var context = new TwitterContext(_authorizer))
                {
                    Status status;
                    if (!Equals(tweet.Latitude, 0d) && !Equals(tweet.Longitude, 0d))
                    {
                        var latitude = (Decimal) tweet.Latitude;
                        var longitude = (Decimal) tweet.Longitude;

                        status = context.UpdateStatus(tweet.Text, latitude, longitude, true);
                    }
                    else
                    {
                        status = context.UpdateStatus(tweet.Text);
                    }

                    completionSource.TrySetResult(GetTweet(JToken.FromObject(status)));
                }
            }
            else
            {
                completionSource.TrySetException(new NotAuthorizedException());
            }

            return await completionSource.Task;
        }

        public override async Task<List<Tweet>> 
            SearchAsync(string query, 
            string geoCode = null, 
            ulong sinceID = 0,
            ulong maxID = 0)
        {
            const int count = 100;

            await CheckAuthorizedAsync();

            var completionSource = new TaskCompletionSource<List<Tweet>>();
            if (_authorizer.IsAuthorized)
            {
                using (var ctx = new TwitterContext(_authorizer))
                {
                    IQueryable<Search> queryable =
                        ctx.Search.Where(search => search.Type == SearchType.Search &&
                                                   search.Query == query &&
                                                   search.ResultType == ResultType.Recent &&
                                                   search.Count == count);

                    if (!Equals(geoCode, "0,0,0km"))
                    {
                        queryable = queryable.Where(search => search.GeoCode == geoCode);
                    }

                    if (sinceID != 0)
                    {
                        queryable = queryable.Where(search => search.SinceID == sinceID);
                    }

                    if (maxID != 0)
                    {
                        queryable = queryable.Where(search => search.MaxID == maxID);
                    }

                    queryable.MaterializedAsyncCallback(delegate(TwitterAsyncResponse<IEnumerable<Search>> response)
                                                        {
                                                            if (response.Status == TwitterErrorStatus.Success)
                                                            {
                                                                Search search = response.State.SingleOrDefault();

                                                                if (search != null)
                                                                {
                                                                    List<Tweet> tweets = GetTweets(search);

                                                                    completionSource.TrySetResult(tweets);
                                                                }
                                                                else
                                                                {
                                                                    completionSource.TrySetResult(new List<Tweet>());
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (response.Exception != null)
                                                                {
                                                                    completionSource.TrySetException(
                                                                        response.Exception);
                                                                }
                                                            }
                                                        });
                }
            }
            else
            {
                completionSource.TrySetResult(new List<Tweet>());
            }

            return await completionSource.Task;
        }

        private async Task CheckAuthorizedAsync()
        {
            if (!IsAuthorized)
            {
                await AuthorizeAsync();
            }
        }

        public override async Task AuthorizeAsync()
        {
            _authorizer = new WinRtAuthorizer
                          {
                              Credentials = new LocalDataCredentials
                                            {
                                                ConsumerKey = Settings.TWITTER_CONSUMER_KEY,
                                                ConsumerSecret = Settings.TWITTER_CONSUMER_SECRET
                                            },
                              UseCompression = true,
                              Callback = new Uri(Settings.TWITTER_CALLBACK)
                          };

            if (!_authorizer.IsAuthorized)
            {
                await _authorizer.AuthorizeAsync();
            }
        }

        public List<Tweet> GetTweets(Search search)
        {
            List<Status> statuses =
                search.Statuses.Where(x => !Equals(x.Coordinates.Latitude, 0.0d) &&
                                           !Equals(x.Coordinates.Longitude, 0.0d)).ToList();

            var tweets = new List<Tweet>();
            foreach (Status status in statuses)
            {
                Tweet tweet = GetTweet(JToken.FromObject(status));
                tweets.Add(tweet);
            }

            return tweets;
        }
        
        #endregion Methods
    }
}
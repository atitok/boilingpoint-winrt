﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable.Geo.DoubleGis;
using Windows.UI.Popups;
using BoilingPoint.Portable;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Foundation;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;

using Caliburn.Micro;
using Telerik.UI.Xaml.Controls.Input;
using Telerik.UI.Xaml.Controls.Input.AutoCompleteBox;

using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Extensions;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.WinRT.Models;
using DataService = BoilingPoint.Portable.Services.DataService;

namespace BoilingPoint.WinRT.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private const String QUERY = "#ihatetowait";
        private const Int32 SEARCH_RADIUS_IN_METERS = 2000;

        private readonly DateTime _defaultDateTime = new DateTime(2000, 1, 1, 0, 0, 0);

        private readonly IDataService _dataService;
        private readonly IGeocoder _geocoder;
        private readonly IGeolocationService _geolocationService;
        private readonly ITwitterService _twitterService;
        private readonly IAnonymousTwitterService _anonymousTwitterService;

        private BindableCollection<TweetPushPinModel> _pins = null;
        private BindableCollection<TweetModel> _tweetModels = null;
        private BindableCollection<TagModel> _tags = null;
        private BindableCollection<FilialModel> _filials { get; set; }

        private String _bank = String.Empty;
        private Int32 _peopleInline = 0;
        private DateTime _waitTime = DateTime.MinValue;
        private Int32 _cashBoxAvailable = 0;
        private Int32 _cashBoxClosed = 0;
        private List<String> _selectedTags = null; 
        private String _message = null;
        private String _countryCode = null;

        private Boolean _isBanksLoading = false;
        private Boolean _isTweetsLoading = false;
        private Boolean _isFilialsLoading = false;

        private WebServiceTextSearchProvider _webSearchProvider = null;

        private GeoPoint _currentGeoPoint = null;

        private Boolean _panelVisible = false;

        private readonly DispatcherTimer _timer = null;

        private Boolean _isLocationEnabled = false;

        #endregion Fields

        #region Constructors

        public MainViewModel(INavigationService navigationService,
            ITwitterService twitterService,
            IAnonymousTwitterService anonymousTwitterService,
            IGeolocationService geolocationService,
            IGeocoder geocoder,
            IDataService dataService)
            : base(navigationService)
        {
            this._twitterService = twitterService;
            this._anonymousTwitterService = anonymousTwitterService;
            this._geolocationService = geolocationService;
            this._geocoder = geocoder;
            this._dataService = dataService;

            this._tweetModels = new BindableCollection<TweetModel>();
            this._pins = new BindableCollection<TweetPushPinModel>();
            this._tags = new BindableCollection<TagModel>();
            this._filials = new BindableCollection<FilialModel>();

            this._selectedTags = new List<String>();

            this._timer = new DispatcherTimer();
        }

        #endregion Constructors

        #region Properties

        public UInt64 SinceID
        {
            get
            {
                UInt64 sinceID = 0;
                TweetModel lastTweet = this.Tweets.FirstOrDefault();
                if (lastTweet != null)
                {
                    sinceID = lastTweet.ID;
                }

                return sinceID;
            }
        }

        public BindableCollection<TweetPushPinModel> Pins
        {
            get { return this._pins; }
            set
            {
                if (Equals(value, this._pins))
                {
                    return;
                }

                this._pins = value;
                this.NotifyOfPropertyChange();
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return this._tweetModels; }
            set
            {
                if (Equals(value, this._tweetModels))
                {
                    return;
                }

                this._tweetModels = value;
                this.NotifyOfPropertyChange();
            }
        }

        public BindableCollection<FilialModel> Filials
        {
            get { return this._filials; }
            set
            {
                if (Equals(this._filials, value))
                {
                    return;
                }

                this._filials = value;
                this.NotifyOfPropertyChange();
            }
        }

        public BindableCollection<TagModel> Tags
        {
            get { return this._tags; }
            set
            {
                if (Equals(value, this._tags))
                {
                    return;
                }

                this._tags = value;
                this.NotifyOfPropertyChange();
            }
        }

        public Boolean IsBanksLoading
        {
            get { return this._isBanksLoading; }
            set
            {
                this._isBanksLoading = value;
                this.NotifyOfPropertyChange();
            }
        }

        public Boolean IsTweetsLoading
        {
            get { return this._isTweetsLoading; }
            set
            {
                this._isTweetsLoading = value;
                this.NotifyOfPropertyChange();
            }
        }

        public Boolean IsFilialsLoading
        {
            get { return this._isFilialsLoading; }
            set
            {
                this._isFilialsLoading = value;
                this.NotifyOfPropertyChange();
            }
        }

        public String Bank
        {
            get { return this._bank; }
            set
            {
                this._bank = value;
                this.NotifyOfPropertyChange(() => this.Bank);
            }
        }

        public Int32 PeopleInline
        {
            get { return this._peopleInline; }
            set
            {
                this._peopleInline = value;
                this.NotifyOfPropertyChange(() => this.PeopleInline);
            }
        }

        public DateTime WaitTime
        {
            get { return this._waitTime; }
            set
            {
                this._waitTime = value;
                this.NotifyOfPropertyChange(() => this.WaitTime);
            }
        }

        public Int32 CashBoxAvailable
        {
            get { return this._cashBoxAvailable; }
            set
            {
                this._cashBoxAvailable = value;
                this.NotifyOfPropertyChange(() => this.CashBoxAvailable);
            }
        }

        public Int32 CashBoxClosed
        {
            get { return this._cashBoxClosed; }
            set
            {
                this._cashBoxClosed = value;
                this.NotifyOfPropertyChange(() => this.CashBoxClosed);
            }
        }

        public String Message
        {
            get { return this._message; }
            set
            {
                this._message = value;
                this.NotifyOfPropertyChange(() => this.Message);
            }
        }

        public Boolean PanelVisible
        {
            get { return this._panelVisible; }
            set
            {
                this._panelVisible = value;
                this.NotifyOfPropertyChange(() => this.PanelVisible);
                this.NotifyOfPropertyChange(() => this.ShowPanelButtonVisible);
            }
        }

        public Boolean ShowPanelButtonVisible
        {
            get { return !this._panelVisible; }
        }

        public List<String> SelectedTags
        {
            get
            {
                if (_selectedTags == null) 
                    _selectedTags = new List<String>();
                return _selectedTags;
            }
            set
            {
                if (Equals(this._selectedTags, value))
                {
                    return;
                }

                this._selectedTags = value;
                this.NotifyOfPropertyChange();
                this.NotifyOfPropertyChange("SelectedTagsString");
            }
        }

        public String SelectedTagsString
        {
            get { return String.Join(" ", this.SelectedTags); }
        }

        public RadAutoCompleteBox BanksAutocompleteBox
        {
            get { return this.FindControl<RadAutoCompleteBox>("Banks"); }
        }

        public RadTimePicker WaitTimePicker
        {
            get { return this.FindControl<RadTimePicker>("WaitTimeControl"); }
        }

        public VisualState ShowPanelState
        {
            get { return this.FindControl<VisualState>("ShowPanelAnimation"); }
        }

        public VisualState HidePanelState
        {
            get { return this.FindControl<VisualState>("HidePanelAnimation"); }
        }

        #endregion Properties

        #region Methods

        protected override async void OnViewLoaded(Object view)
        {
            if (this._geolocationService.Status == GeolocationStatus.Ready)
            {
                this._currentGeoPoint = await this._geolocationService.GetGeoPointAsync();
            }

            await this.InitializeDoubleGisAsync();

            await this.LoadTweetsAsync();

            await this.InitializeControlsAsync();

            await this.LoadFilialsAsync();

            this._timer.Interval = TimeSpan.FromSeconds(60);
            this._timer.Tick += this.OnTimerTick;
            this._timer.Start();
        }

        private async Task InitializeDoubleGisAsync()
        {
            Config config = await _dataService.GetConfigAsync();

            DoubleGis.Initialize(config.DoubleGisApiKey);
        }

        protected async Task SendTweet()
        {
            if (!this._twitterService.IsAuthorized)
            {
                await this._twitterService.AuthorizeAsync();
            }

            Tweet tweet = this.GetTweet();

            GeoPoint geoPoint = await this._geolocationService.GetGeoPointAsync();
            if (geoPoint != null)
            {
                tweet.Latitude = geoPoint.Latitude;
                tweet.Longitude = geoPoint.Longitude;
            }

            await this._twitterService.SendTweetAsync(tweet);

            this.ClearNewTweetControls();

            this.HidePanel();

            await this.LoadTweetsAsync(this.SinceID);
        }

        private async Task InitializeControlsAsync()
        {
            this.WaitTime = this._defaultDateTime;

            this._webSearchProvider = new WebServiceTextSearchProvider();
            this._webSearchProvider.InputChanged += this.OnSearchProviderInputChanged;

            this.BanksAutocompleteBox.InitializeSuggestionsProvider(this._webSearchProvider);
            
            List<Tag> tags = await this._dataService.GetPredefinedTagsAsync();
            this.Tags.AddRange(tags.Select(q => new TagModel(q)));

            ComboBox cbxTags = this.FindControl<ComboBox>("TagsComboBox");
            cbxTags.ItemsSource = new[] { this.Tags };
        }

        private async void OnSearchProviderInputChanged(Object sender, EventArgs e)
        {
            this.IsBanksLoading = true;

            WebServiceTextSearchProvider provider = sender as WebServiceTextSearchProvider;
            if (provider == null)
            {
                return;
            }

            String inputString = provider.InputString;

            String countryCode = await this.GetCountryCodeAsync();
            if (!String.IsNullOrEmpty(countryCode))
            {
                List<Bank> banks = await this._dataService.SearchBanksByTitleAsync(inputString, countryCode);
                provider.LoadItems(banks);
            }

            this.IsBanksLoading = false;
        }

        private async Task LoadTweetsAsync(UInt64 sinceID = 0)
        {
            this.IsTweetsLoading = true;

            IEnumerable<Tweet> tweets = null;

            tweets = await this._anonymousTwitterService.SearchAsync(QUERY, sinceID);

            IEnumerable<TweetModel> tweetModels = tweets.Select(q => new TweetModel(q))
                .OrderBy(q => q.CreatedAtUtc);

            foreach (TweetModel tweetModel in tweetModels)
            {
                this.Tweets.Insert(0, tweetModel);
            }

            this.IsTweetsLoading = false;

            this.InitializeMapPins(tweets);
        }

        private async Task LoadFilialsAsync(Int32 radiusInMeters = SEARCH_RADIUS_IN_METERS)
        {
            this.IsFilialsLoading = true;

            GeoPoint geoPoint = this._currentGeoPoint;

            if (geoPoint != null)
            {
                List<Filial> filials =
                    (await this._dataService.SearchFilialsAsync(geoPoint.Latitude, geoPoint.Longitude, radiusInMeters))
                        .OrderBy(q => q.Distance)
                        .ToList();

                String countryCode = await this.GetCountryCodeAsync();
                if (!String.IsNullOrEmpty(countryCode))
                {
                    List<Bank> banks = await this._dataService.GetBanksAsync(countryCode);
                }

                this.Filials.AddRange(filials.Select(q => new FilialModel(q)));
            }

            this.IsFilialsLoading = false;
        }

        private void InitializeMapPins(IEnumerable<Tweet> tweets)
        {
            foreach (Tweet tweet in tweets)
            {
                TweetPushPinModel pushPin = this.Pins.FirstOrDefault(q => q.Grouping.CanAdd(tweet));
                if (pushPin != null)
                {
                    pushPin.AddTweetToGrouping(tweet);
                }
                else
                {
                    TweetGrouping grouping = new TweetGrouping(DataService.DEFAULT_RADIUS_IN_METERS) { tweet };
                    pushPin = new TweetPushPinModel(grouping);
                    this.Pins.Add(pushPin);
                }
            }
        }

        private Tweet GetTweet()
        {
            Tweet tweet = new Tweet()
            {
                Bank = new Bank() {Title = this.Bank},
                PeopleInline = this.PeopleInline,
                WaitTime = (this.WaitTime - this._defaultDateTime).TotalMinutes,
                CashBoxAvailable = this.CashBoxAvailable,
                CashBoxClosed = this.CashBoxClosed,
                Comment = this.Message,
                Tags = this.SelectedTags.Select(q => q.Substring(1)).ToList()
            };

            return tweet;
        }

        private async Task<String> GetCountryCodeAsync()
        {
            if (this._countryCode == null)
            {
                String countryCode = null;

                if (this._currentGeoPoint != null)
                {
                    countryCode = await this._geocoder.GetCountryCodeAsync(
                        this._currentGeoPoint.Latitude, this._currentGeoPoint.Longitude);
                }

                this._countryCode = countryCode;
            }

            return this._countryCode;
        }

        private void ClearNewTweetControls()
        {
            this.Bank = String.Empty;
            this.PeopleInline = 0;
            this.WaitTime = this._defaultDateTime;
            this.CashBoxAvailable = 0;
            this.CashBoxClosed = 0;
            this.Message = String.Empty;
        }

        private async void OnTimerTick(Object sender, Object e)
        {
            foreach (TweetModel tweetModel in this.Tweets)
            {
                tweetModel.CreatedAtTimeago = tweetModel.CreatedAtUtc.ToTimeago();
            }

            await this.LoadTweetsAsync(this.SinceID);
        }

        private void ShowPanel()
        {
            this.ShowPanelState.Storyboard.Begin();
            this.PanelVisible = true;
        }

        private void HidePanel()
        {
            this.HidePanelState.Storyboard.Begin();
            this.PanelVisible = false;
        }

        private void ShowHidePanel()
        {
            if (this.PanelVisible)
            {
                this.HidePanel();
            }
            else
            {
                this.ShowPanel();
            }
        }

        private void CancelTweet()
        {
            this.HidePanel();
            this.ClearNewTweetControls();
        }

        private void OnSelectTagsTapped(ComboBox tagsComboBox, ListBox tagsListBox)
        {
            this.SelectedTags = tagsListBox.SelectedItems.Cast<TagModel>()
                .Select(q => q.Name).ToList();

            tagsComboBox.IsDropDownOpen = false;
        }

        private void OnCancelTagsTapped(ComboBox tagsComboBox)
        {
            tagsComboBox.IsDropDownOpen = false;
        }

        private async void OnFilialTapped(FrameworkElement sender, FilialModel filial)
        {
            this._navigationService.UriFor<ProfileViewModel>()
                .WithParam(q => q.ProfileId, filial.Id)
                .Navigate();
        }

        private void GoToMap()
        {
            this._navigationService.UriFor<MapViewModel>()
                .WithParam(q => q.CurrentLocation, this._currentGeoPoint)
                .Navigate();
        }

        #endregion Methods
    }
}
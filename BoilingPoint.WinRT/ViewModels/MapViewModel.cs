﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;

using Caliburn.Micro;
using Yandex.Maps;
using Yandex.Positioning;

using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.Portable.Extensions;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.WinRT.Models;

namespace BoilingPoint.WinRT.ViewModels
{
    using System.Windows.Input;

    public class MapViewModel : ViewModelBase
    {
        #region Fields

        private const int SEARCH_RADIUS_IN_METERS = 1000;

        private readonly IDataService _dataService = null;
        private readonly IGeolocationService _geolocationService = null;
        private readonly ITwitterService _twitterService = null;
        private readonly IAnonymousTwitterService _anonymousTwitterService = null;

        private BindableCollection<TweetModel> _tweets = null;
        private BindableCollection<PushPinModel> _pushPins = null;

        private MapLayer _pushPinsLayer = null;
        private DataTemplate _profilePushpinTemplate;
        private DataTemplate _tweetPushpinTemplate;

        #endregion Fields

        #region Constructors

        public MapViewModel(INavigationService navigationService,
            IDataService dataService,
            IGeolocationService geolocationService,
            ITwitterService twitterService,
            IAnonymousTwitterService anonymousTwitterService)
            : base(navigationService)
        {
            this._dataService = dataService;
            this._geolocationService = geolocationService;
            this._twitterService = twitterService;
            this._anonymousTwitterService = anonymousTwitterService;

            this._tweets = new BindableCollection<TweetModel>();
            this._pushPins = new BindableCollection<PushPinModel>();
        }

        #endregion Constructors

        #region Properties

        public UInt64 TweetID { get; set; }

        public String ProfileID { get; set; }

        public GeoPoint CurrentLocation { get; set; }

        public UInt64 SinceID
        {
            get
            {
                UInt64 sinceID = 0;
                TweetModel lastTweet = this.Tweets.FirstOrDefault();
                if (lastTweet != null)
                {
                    sinceID = lastTweet.ID;
                }

                return sinceID;
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return this._tweets; }
            set
            {
                if (Equals(this._tweets, value))
                {
                    return;
                }

                this._tweets = value;
                this.NotifyOfPropertyChange();
            }
        }

        public BindableCollection<PushPinModel> PushPins
        {
            get { return this._pushPins; }
            set
            {
                if (Equals(this._pushPins, value))
                {
                    return;
                }

                this._pushPins = value;
                this.NotifyOfPropertyChange();
            }
        }

        public Map Map
        {
            get { return this.FindControl<Map>("MapControl"); }
        }

        #endregion Properties

        #region Methods

        protected override async void OnViewLoaded(Object view)
        {
            this._tweetPushpinTemplate = (DataTemplate)Application.Current.Resources["TweetPushpinTemplate"];
            this._profilePushpinTemplate = (DataTemplate)Application.Current.Resources["ProfilePushpinTemplate"];
            this._pushPinsLayer = (MapLayer)this.Map.FindName("PushpinsLayer");

            if (this.CurrentLocation == null && this._geolocationService.Status == GeolocationStatus.Ready)
            {
                this.CurrentLocation = await this._geolocationService.GetGeoPointAsync();
            }

            this.LoadTweets();

            await this.LoadProfilesAsync();

            if (TweetID != 0)
            {
                JumpToTweet();
            }
            else if (!string.IsNullOrEmpty(this.ProfileID))
            {
                JumpToProfile();
            }
            else
            {
                if (this.CurrentLocation != null)
                {
                    this.JumpToPosition(this.CurrentLocation);
                }
            }
        }

        private void LoadTweets()
        {
            List<TweetGrouping> groupings =
                _twitterService.GroupTweetsByDistance(_anonymousTwitterService.Tweets);

            IEnumerable<TweetPushPinModel> pushpins =
                groupings.Select(grouping => new TweetPushPinModel(grouping)).ToList();

            foreach (TweetPushPinModel pushpinModel in pushpins)
            {
                AddPushpin(pushpinModel, _tweetPushpinTemplate);
            }

            this.PushPins.AddRange(pushpins.Cast<PushPinModel>());
        }

        private async Task LoadProfilesAsync()
        {
            if (this._geolocationService.Status == GeolocationStatus.Ready)
            {
                GeoPoint geoPoint = await this._geolocationService.GetGeoPointAsync();
                double latitude = geoPoint.Latitude;
                double longitude = geoPoint.Longitude;

                var profilePushpins = new List<ProfilePushpinModel>();
                List<Profile> profiles =
                    await _dataService.SearchProfilesAsync(latitude, longitude, SEARCH_RADIUS_IN_METERS);
                foreach (
                    ProfilePushpinModel pushpinModel in profiles.Select(profile => new ProfilePushpinModel(profile)))
                {
                    AddPushpin(pushpinModel, _profilePushpinTemplate);

                    profilePushpins.Add(pushpinModel);
                    this.PushPins.Add(pushpinModel);
                }

                List<Bank> banks = await _dataService.SearchBanksAsync(profiles);

                List<Profile> profilesWithImages = await GetProfilesWithImagesAsync(banks);
                foreach (Profile profile in profilesWithImages)
                {
                    ProfilePushpinModel pushpinModel =
                        profilePushpins.First(x => x.Id == profile.Id);
                    if (pushpinModel != null)
                    {
                        pushpinModel.ImageUri = profile.ImageUri;
                    }
                }
            }
        }

        private async Task<List<Profile>> GetProfilesWithImagesAsync(List<Bank> banks)
        {
            var profilesWithImages = new List<Profile>();
            var userScreenNames = new List<string>();
            if (banks != null)
            {
                foreach (Bank bank in banks.Where(x => !string.IsNullOrEmpty(x.TwitterUsername) &&
                                                       !userScreenNames.Contains(x.TwitterUsername)))
                {
                    userScreenNames.Add(bank.TwitterUsername);
                }

                List<User> users = await _anonymousTwitterService.GetUsersAsync(string.Join(",", userScreenNames));

                foreach (User user in users)
                {
                    string fileName = Path.GetFileName(user.ProfileImageUrl);
                    if (fileName == null || fileName.StartsWith("default_profile")) continue;
                    Bank bank = banks.First(x => Equals(x.TwitterUsername, user.ScreenName));
                    Profile profile = bank.Profile;
                    profile.ImageUri = new Uri(user.ProfileImageUrl);
                    profilesWithImages.Add(profile);
                }
            }

            return profilesWithImages;
        }

        private void AddPushpin(PushPinModel pushpinModel, DataTemplate template)
        {
            var pushPin =
                new PushPin
                {
                    DataContext = pushpinModel,
                    ContentTemplate = template,
                    State = pushpinModel.State,
                    ContentVisibility = pushpinModel.ContentVisibility,
                    Visibility = pushpinModel.Visibility
                };

            MapLayer.SetLocation(pushPin, pushpinModel.Location);
            MapLayer.SetAlignment(pushPin, Alignment.BottomCenter);

            Canvas.SetZIndex(pushPin, pushpinModel.ZIndex);

            this._pushPinsLayer.Children.Add(pushPin);
        }

        private void JumpToProfile()
        {
            if (this.PushPins != null)
            {
                foreach (PushPinModel pushpinModel in this.PushPins)
                {
                    if (!(pushpinModel is ProfilePushpinModel)) continue;
                    Profile profile = ((ProfilePushpinModel)pushpinModel).Profile;
                    if (profile.Id != this.ProfileID) continue;
                    JumpToPosition(profile.Latitude, profile.Longitude);
                    pushpinModel.Visibility = Visibility.Visible;
                    break;
                }
            }
        }

        private void JumpToTweet()
        {
            if (this.PushPins != null)
            {
                foreach (PushPinModel pushpinModel in this.PushPins)
                {
                    if (!(pushpinModel is TweetPushPinModel)) continue;
                    Tweet tweet = ((TweetPushPinModel)pushpinModel).Grouping.FirstOrDefault(t => t.ID == TweetID);
                    if (tweet == null) continue;
                    JumpToPosition(tweet.Latitude, tweet.Longitude);
                    pushpinModel.Visibility = Visibility.Visible;
                    break;
                }
            }
        }

        private void JumpToPosition(GeoPoint geoPoint)
        {
            if (geoPoint != null)
            {
                this.Map.Center = new GeoCoordinate(geoPoint.Latitude, geoPoint.Longitude);
            }
        }

        private void JumpToPosition(Double latitude, Double longitude)
        {
            this.Map.Center = new GeoCoordinate(latitude, longitude);
        }

        private void MapZoomIn()
        {
            this.Map.ZoomIn();
        }

        private void MapZoomOut()
        {
            this.Map.ZoomOut();
        }

        private async void MapFindMe()
        {
            GeoPositionStatus status = this.Map.JumpToCurrentLocation();
            switch (status)
            {
                case GeoPositionStatus.Disabled:
                    MessageDialog alert = new MessageDialog(App.GetResource("LocationIsDisabled"));
                    await alert.ShowAsync();
                    break;
            }
        }

        private void CollapseAllPushPins()
        {
            this.Map.PushPinManager.CollapseAllPushpins();
        }

        private void GoBack()
        {
            if (this._navigationService.CanGoBack)
            {
                this._navigationService.GoBack();
            }
        }

        #endregion Methods
    }
}

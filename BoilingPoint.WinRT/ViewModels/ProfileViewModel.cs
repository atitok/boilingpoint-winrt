﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

using Caliburn.Micro;

using BoilingPoint.Portable.Services;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.WinRT.ViewModels
{
    using Windows.System;
    using Windows.UI.Xaml;

    /// <summary>
    /// Profile view model.
    /// </summary>
    public class ProfileViewModel : ViewModelBase
    {
        #region Fields

        private readonly IDataService _dataService = null;
        private readonly IAnonymousTwitterService _anonymousTwitterService = null;

        private ProfileModel _profile = null;
        private FilialModel _filial = null;
        private BindableCollection<TweetModel> _tweets = null;
        private BindableCollection<ContactModel> _contacts = null;

        #endregion Fields

        #region Constructors

        public ProfileViewModel(INavigationService navigationService,
            IDataService dataService,
            IAnonymousTwitterService anonymousTwitterService)
            : base(navigationService)
        {
            this._dataService = dataService;
            this._anonymousTwitterService = anonymousTwitterService;

            this._tweets = new BindableCollection<TweetModel>();
            this._contacts = new BindableCollection<ContactModel>();
        }

        #endregion Constructors

        #region Properties

        public String ProfileId { get; set; }

        public ProfileModel Profile
        {
            get { return this._profile; }
            set
            {
                if (Equals(this._profile, value))
                {
                    return;
                }

                this._profile = value;
                this.NotifyOfPropertyChange();
            }
        }

        public FilialModel Filial
        {
            get { return this._filial; }
            set
            {
                if (Equals(this._filial, value))
                {
                    return;
                }

                this._filial = value;
                this.NotifyOfPropertyChange();
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return this._tweets; }
            set
            {
                if (Equals(this._tweets, value))
                {
                    return;
                }

                this._tweets = value;
                this.NotifyOfPropertyChange();
            }
        }

        public BindableCollection<ContactModel> Contacts
        {
            get { return this._contacts; }
            set
            {
                if (Equals(this._contacts, value))
                {
                    return;
                }

                this._contacts = value;
                this.NotifyOfPropertyChange();
            }
        }

        #endregion Properties

        #region Methods

        protected override async void OnViewLoaded(Object view)
        {
            if (!String.IsNullOrEmpty(this.ProfileId))
            {
                Profile profile = await this._dataService.GetProfileByIdAsync(this.ProfileId);
                if (profile != null)
                {
                    this.Profile = new ProfileModel(profile);

                    this.Contacts.AddRange(profile.Contacts.Select(q => new ContactModel(q)));
                }

                List<Tweet> tweets = await this._dataService.GetTweetsByProfileIdAsync(this.ProfileId);
                
                this.Tweets.AddRange(tweets.Select(q => new TweetModel(q)));
            }
        }

        private void GoBack()
        {
            if (this._navigationService.CanGoBack)
            {
                this._navigationService.GoBack();
            }
        }

        #endregion Methods
    }
}

﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Caliburn.Micro;

namespace BoilingPoint.WinRT.ViewModels
{
    public abstract class ViewModelBase : Screen
    {
        protected readonly INavigationService _navigationService;

        protected readonly Dictionary<String,Object> _controlsCache = new Dictionary<String, Object>();

        protected ViewModelBase(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public Boolean CanGoBack
        {
            get { return _navigationService.CanGoBack; }
        }

        public void GoBack()
        {
            _navigationService.GoBack();
        }

        public T FindControl<T>(String name)
        {
            Object control = null;

            if (!this._controlsCache.TryGetValue(name, out control))
            {
                control = ((FrameworkElement)this.GetView()).FindName(name);
                this._controlsCache.Add(name, control);
            }

            return (T) control;
        }
    }
}